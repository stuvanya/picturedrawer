#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "square.h"
#include "circle.h"
#include "triangle.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);

private slots:
    void drawSquare();
    void drawCircle();
    void drawTriangle();
    void clear();

private:
    std::vector<Figure*> figures;
};
#endif // MAINWINDOW_H

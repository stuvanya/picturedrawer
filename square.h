#ifndef SQUARE_H
#define SQUARE_H

#include "figure.h"

class Square : public Figure
{

public:
    Square(QWidget *parent = 0);

protected:
    void doPainting() override;
};

#endif // SQUARE_H

#include <QPainter>
#include "circle.h"

Circle::Circle(QWidget *parent)
    : Figure(parent)
{ }

void Circle::doPainting()
{
    QRectF rectangle(0, 0, 100.0, 100.0);
    QPainter painter(this);
    painter.setPen(QColor("#1ac500"));
    painter.setBrush(QBrush("#539e47"));
    painter.drawEllipse(rectangle);
}

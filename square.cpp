#include "square.h"

Square::Square(QWidget *parent)
    : Figure(parent)
{ }

void Square::doPainting()
{
    QRectF rectangle(0, 0, 100.0, 100.0);
    QPainter painter(this);
    painter.setPen(QColor("#d4d4d4"));
    painter.setBrush(QBrush("#c56c00"));
    painter.drawRect(rectangle);
}

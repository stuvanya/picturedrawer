#ifndef CIRCLE_H
#define CIRCLE_H

#include "figure.h"

class Circle : public Figure
{

public:
    Circle(QWidget *parent = 0);

protected:
    void doPainting() override;
};

#endif // CIRCLE_H

#include "figure.h"

Figure::Figure(QWidget *parent)
    : QWidget(parent)
{ }

void Figure::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e);

    doPainting();
}

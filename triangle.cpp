#include "triangle.h"

Triangle::Triangle(QWidget *parent)
    : Figure(parent)
{ }

void Triangle::doPainting()
{
    QPointF points[3] = {
        QPointF(10.0, 80.0),
        QPointF(20.0, 10.0),
        QPointF(80.0, 30.0)
    };
    QPainter painter(this);
    painter.setPen(QColor("#004fc5"));
    painter.setBrush(QBrush("#c50024"));
    painter.drawPolygon(points, 3);
}

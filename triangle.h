#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "figure.h"

class Triangle : public Figure
{

public:
    Triangle(QWidget *parent = 0);

protected:
    void doPainting() override;
};

#endif // TRIANGLE_H

#include <QPushButton>
#include <QPainter>
#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setWindowTitle("Рисователь фигур");
    resize(620, 620);

    QPushButton * sqButton = new QPushButton(("Нарисовать квадрат"), this);
    sqButton->setGeometry(20, 50, 180, 24);
    connect(sqButton, SIGNAL(clicked()), this, SLOT(drawSquare()));

    QPushButton * trButton = new QPushButton(("Нарисовать треугольник"), this);
    trButton->setGeometry(220, 50, 180, 24);
    connect(trButton, SIGNAL(clicked()), this, SLOT(drawTriangle()));

    QPushButton * ciButton = new QPushButton(("Нарисовать круг"), this);
    ciButton->setGeometry(420, 50, 180, 24);
    connect(ciButton, SIGNAL(clicked()), this, SLOT(drawCircle()));

    QPushButton * clButton = new QPushButton(("Очистить"), this);
    clButton->setGeometry(20, 550, 180, 24);
    connect(clButton, SIGNAL(clicked()), this, SLOT(clear()));
}

void MainWindow::drawSquare()
{
    Square* square = new Square(this);
    square->show();
    square->resize(200, 200);
    square->move(100, 200);
    figures.push_back(square);
}

void MainWindow::drawTriangle()
{
    Triangle* triangle = new Triangle(this);
    triangle->show();
    triangle->resize(200, 200);
    triangle->move(250, 200);
    figures.push_back(triangle);
}

void MainWindow::clear()
{
    for(auto figure : figures)
    {
        figure->deleteLater();
    }
    figures.clear();
}

void MainWindow::drawCircle()
{
    Circle* circle = new Circle(this);
    circle->show();
    circle->resize(200, 200);
    circle->move(400, 200);
    figures.push_back(circle);
}


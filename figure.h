#ifndef FIGURE_H
#define FIGURE_H

#include <QWidget>
#include <QPainter>

class Figure: public QWidget
{
public:
    Figure(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent *e);
    virtual void doPainting() = 0;
};

#endif // FIGURE_H
